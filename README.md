# Mern Test Api

javascript practice, mern stack based project

## Contents
- [ Dockerfile ](#Dockerfile)
- [ Dev Environment ](#DevEnvironment)
    - [ Build ](#Build)
        - [ Frontend ](#Frontend)
        - [ Brontend ](#Backend)


## Dockerfile

The dockerfile can be built 2 ways
  - with the default node:16 as base
  - with node:16-alpine as base

Size difference is rather significant after npm package installation:

![container image size difference](img/node_alpineVSbase-size_diff.png)

Due to this I have elected to use the alpine image as the base

## Dev Environment

### Build

#### Frontend
____

```
cd frontend
docker build -t web .
docker run -d --name web -p 8080:8080 mern-web
```

#### Backend
____
```
cd backend
docker build -t api .
docker run -d --name api -p 3000:3000 mern-api
```
